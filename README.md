# Teste Prático - Iniflex

## Tecnologias Utilizadas


- **Spring Boot:** Framework que simplifica o desenvolvimento de aplicativos Java.
- **Lombok:** Biblioteca que facilita a criação de classes reduzindo a quantidade de código boilerplate.
- **Java 17:** A versão mais recente da linguagem Java.
- **H2 (Banco de Dados em Memória):** Banco de dados leve e em memória, ideal para ambientes de teste.
- **JPA (Java Persistence API):** Especificação do Java para gerenciamento de dados em sistemas de software.

## Como Clonar o Projeto

1. Abra o terminal e navegue até o diretório onde deseja clonar o projeto.

   ```bash

   Clone o projeto 

   git clone https://gitlab.com/EduardoGautier/teste-pratico-iniflex.git

   Entre no diretório do projeto.
 
   cd teste-pratico-iniflex

   Execute sua aplicação

   ./gradlew bootRun


## Como Testar a API usando Postman 
1. Certifique-se de que a aplicação está em execução.

2. Abra o Postman.

3. Importe a coleção Postman fornecida.

5. No Postman, clique em "Import" na barra lateral esquerda.
6. Selecione a opção "Import From Link".
7. Cole o link da coleção: Test.postman_collection.json.
8. A coleção deve aparecer na barra lateral esquerda do Postman.

9. Execute as requisições da coleção para testar os endpoints.



