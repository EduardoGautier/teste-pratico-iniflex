package com.example.projectData.demo.iniflix.repository;

import com.example.projectData.demo.iniflix.entity.Funcionario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface FuncionarioRepository extends CrudRepository<Funcionario, Long> {

    Optional<Funcionario> findByNome(String nome);

    List<Funcionario> findAllByOrderByNomeAsc();

    @Query("SELECT SUM(f.salario) FROM Funcionario f")
    BigDecimal calcularTotalSalarios();




}
