package com.example.projectData.demo.iniflix;

import com.example.projectData.demo.iniflix.entity.Funcionario;
import com.example.projectData.demo.iniflix.repository.FuncionarioRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.time.LocalDate;

@SpringBootApplication
public class IniflixApplication {

	public static void main(String[] args) {
		SpringApplication.run(IniflixApplication.class, args);
	}


	@Bean
	public CommandLineRunner demo(FuncionarioRepository repository) {
		return (args) -> {
			// save a few customers
			repository.save(new Funcionario("Maria", LocalDate.of(2000, 10, 18), BigDecimal.valueOf(2009.44), "Operador"));
			repository.save(new Funcionario("Joao", LocalDate.of(1990, 5, 12), BigDecimal.valueOf(2284.38), "Operador"));
			repository.save(new Funcionario("Caio", LocalDate.of(1961, 5, 2), BigDecimal.valueOf(9836.14), "Coordenador"));
			repository.save(new Funcionario("Miguel", LocalDate.of(1988, 10, 14), BigDecimal.valueOf(19119.88), "Diretor"));
			repository.save(new Funcionario("Alice", LocalDate.of(1995, 1, 5), BigDecimal.valueOf(2234.68), "Recepcionista"));
			repository.save(new Funcionario("Heitor", LocalDate.of(1999, 11, 19), BigDecimal.valueOf(1582.72), "Operador"));
			repository.save(new Funcionario("Arthur", LocalDate.of(1993, 3, 31), BigDecimal.valueOf(4071.84), "Contador"));
			repository.save(new Funcionario("Laura", LocalDate.of(1994, 7, 8), BigDecimal.valueOf(3017.45), "Gerente"));
			repository.save(new Funcionario("Heloísa", LocalDate.of(2003, 5, 24), BigDecimal.valueOf(1606.85), "Eletricista"));
			repository.save(new Funcionario("Helena", LocalDate.of(1996, 9, 2), BigDecimal.valueOf(2799.93), "Gerente"));

		};
	}
}
