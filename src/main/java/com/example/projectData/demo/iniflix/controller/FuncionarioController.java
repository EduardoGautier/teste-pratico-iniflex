package com.example.projectData.demo.iniflix.controller;


import com.example.projectData.demo.iniflix.entity.Funcionario;
import com.example.projectData.demo.iniflix.repository.FuncionarioRepository;
import com.example.projectData.demo.iniflix.service.FuncionarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("")
public class FuncionarioController {
    @Autowired
    private FuncionarioService funcionarioService;

    @Autowired
    FuncionarioRepository crepository;

    @GetMapping("/inserirFuncionarios")
    public Iterable<Funcionario> inserirFuncionarios() {
        return funcionarioService.inserirFuncionarios();
    }

    @GetMapping("/removerFuncionario")
    public Iterable<Funcionario> removerFuncionario() {
        return funcionarioService.removerFuncionario();
    }

    @GetMapping("/imprimirFuncionarios")
    public Iterable<Funcionario> imprimirFuncionarios() {
        return funcionarioService.imprimirFuncionarios();
    }

    @GetMapping("/aumentarSalario")
    public Iterable<Funcionario> aumentarSalario() {
        return funcionarioService.aumentarSalario();
    }

    @GetMapping("/agruparPorFuncao")
    public Map<String, List<Funcionario>> agruparPorFuncao() {
        return funcionarioService.agruparPorFuncao();
    }

    @GetMapping("/aniversariantesMes")
    public List<Funcionario> aniversariantesMes() {
        return funcionarioService.aniversariantesMes();
    }

    @GetMapping("/funcionarioMaisVelho")
    public Funcionario funcionarioMaisVelho() {
        return funcionarioService.funcionarioMaisVelho();
    }

    @GetMapping("/ordenarPorNome")
    public List<Funcionario> ordenarPorNome() {
        return funcionarioService.ordenarPorNome();
    }

    @GetMapping("/totalSalarios")
    public BigDecimal totalSalarios() {
        return funcionarioService.totalSalarios();
    }

    @GetMapping("/salarioPorMinimo")
    public Map<String, BigDecimal> salarioPorMinimo() {
        return funcionarioService.salarioPorMinimo();
    }
}
