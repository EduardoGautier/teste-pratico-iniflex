package com.example.projectData.demo.iniflix.service;


import com.example.projectData.demo.iniflix.entity.Funcionario;
import com.example.projectData.demo.iniflix.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class FuncionarioService {
    private List<Funcionario> funcionarios;
    @Autowired
    private FuncionarioRepository repository;

    public Iterable<Funcionario> inserirFuncionarios() {
        return repository.findAll();
    }

    public Iterable<Funcionario> removerFuncionario() {
        Optional<Funcionario> funcionarioOptional = repository.findByNome("Joao");

        funcionarioOptional.ifPresent(funcionario -> {
            repository.delete(funcionario);
            System.out.println("Funcionário removido: " + funcionario);
        });

        return repository.findAll();
    }

    public Iterable<Funcionario> imprimirFuncionarios() {
        return repository.findAll();
    }

    public Iterable<Funcionario> aumentarSalario() {
        Iterable<Funcionario> funcionarios = repository.findAll();

        funcionarios.forEach(funcionario -> {
            BigDecimal novoSalario = funcionario.getSalario().multiply(new BigDecimal("1.1"));
            funcionario.setSalario(novoSalario);
        });

        // Salvar as alterações no banco de dados
        repository.saveAll(funcionarios);

        return funcionarios;
    }


    public Map<String, List<Funcionario>> agruparPorFuncao() {
        Iterable<Funcionario> funcionariosIterable = repository.findAll();

        // Converter o Iterable para uma lista
        List<Funcionario> funcionarios = new ArrayList<>();
        funcionariosIterable.forEach(funcionarios::add);

        return funcionarios.stream()
                .collect(Collectors.groupingBy(Funcionario::getFuncao));
    }

    public List<Funcionario> aniversariantesMes() {
        LocalDate mes10 = LocalDate.of(LocalDate.now().getYear(), 10, 1);
        LocalDate mes12 = LocalDate.of(LocalDate.now().getYear(), 12, 1);

        Iterable<Funcionario> funcionariosIterable = repository.findAll();

        List<Funcionario> funcionarios = new ArrayList<>();
        funcionariosIterable.forEach(funcionarios::add);

        return funcionarios.stream()
                .filter(funcionario ->
                        funcionario.getDataNascimento().getMonth().equals(mes10.getMonth()) ||
                                funcionario.getDataNascimento().getMonth().equals(mes12.getMonth()))
                .collect(Collectors.toList());
    }

    public Funcionario funcionarioMaisVelho() {
        Iterable<Funcionario> funcionariosIterable = repository.findAll();

        Optional<Funcionario> funcionarioMaisVelho = StreamSupport.stream(funcionariosIterable.spliterator(), false)
                .filter(funcionario -> funcionario.getDataNascimento() != null)
                .max(Comparator.comparing(Funcionario::getDataNascimento));

        return funcionarioMaisVelho.orElse(null);
    }

    public List<Funcionario> ordenarPorNome() {
        return repository.findAllByOrderByNomeAsc();
    }

    public BigDecimal totalSalarios() {
        return repository.calcularTotalSalarios();

    }


    public Map<String, BigDecimal> salarioPorMinimo() {
        BigDecimal salarioMinimo = new BigDecimal("1212.00");

        Iterable<Funcionario> funcionariosIterable = repository.findAll();

        return StreamSupport.stream(funcionariosIterable.spliterator(), false)
                .collect(Collectors.toMap(Funcionario::getNome, funcionario ->
                        funcionario.getSalario().divide(salarioMinimo, 2, BigDecimal.ROUND_DOWN)));
    }
}
